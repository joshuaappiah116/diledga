# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :diledga,
  ecto_repos: [Diledga.Repo]

# Configures the endpoint
config :diledga, DiledgaWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "b3LT94fq1SW/Yszy0RB/LqjqcPDN95ZVBDe7Zp2Jw/zhtsyqZYVJRELutrD1f46s",
  render_errors: [view: DiledgaWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Diledga.PubSub, adapter: Phoenix.PubSub.PG2]


config :logger,
  backends: [
    :console,
    {Diledga.Audit.DiskLogBackend, :disk_log_backend}
  ],
 level: :info

 config :logger, :disk_log_backend,
  level: :debug,
  metadata: [:user, :id],
  max_file_size: 1000,
  max_no_files: 10


# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id],
  level: :info

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
