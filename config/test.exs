use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :diledga, DiledgaWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger,
  backends: [
    :console
    #{Diledga.Audit.DiskLogBackend, :disk_log_backend}
  ],
 level: :info

 config :logger, :disk_log_backend,
  level: :info,
  metadata: [:file, :line],
  max_file_size: 10 * 1_000_000,
  max_no_files: 10


  config :diledga, Identity.Certificate.Authority,
    home_dir: "certificate_authority",
    auth_dir: "auth_dir",
    priv_key: "auth_key",
    rdn: "/C=US/ST=CA/L=San Francisco/O=Acme/CN=ECDSA Root CA",
    secret: "somesecretinformationthatmustbekeptasecret",
    best_before: 365 * 40,
    level: 1 #currently ignored but might be used for security reasons in the near future.
             #level currently defaults to 1.




# Configure your database
config :diledga, Diledga.Repo,
  username: "postgres",
  password: "Esther1998",
  database: "diledga_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
