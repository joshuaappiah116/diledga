defmodule DiledgaWeb.WalletView do
  use DiledgaWeb, :view
  alias DiledgaWeb.WalletView

  def render("index.json", %{wallets: wallets}) do
    %{data: render_many(wallets, WalletView, "wallet.json")}
  end

  def render("show.json", %{wallet: wallet}) do
    %{data: render_one(wallet, WalletView, "wallet.json")}
  end

  def render("wallet.json", %{wallet: wallet}) do
    %{id: wallet.id,
      name: wallet.name,
      public_name: wallet.public_name,
      address: wallet.address,
      code: wallet.code}
  end
end
