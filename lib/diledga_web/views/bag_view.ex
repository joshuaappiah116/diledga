defmodule DiledgaWeb.BagView do
  use DiledgaWeb, :view
  alias DiledgaWeb.BagView

  def render("index.json", %{bags: bags}) do
    %{data: render_many(bags, BagView, "bag.json")}
  end

  def render("show.json", %{bag: bag}) do
    %{data: render_one(bag, BagView, "bag.json")}
  end

  def render("bag.json", %{bag: bag}) do
    %{id: bag.id,
      name: bag.name,
      code: bag.code}
  end
end
