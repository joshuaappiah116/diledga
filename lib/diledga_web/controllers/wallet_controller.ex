defmodule DiledgaWeb.WalletController do
  use DiledgaWeb, :controller

  alias Diledga.Identity
  alias Diledga.Identity.Wallet

  action_fallback DiledgaWeb.FallbackController

  def index(conn, _params) do
    wallets = Identity.list_wallets()
    render(conn, "index.json", wallets: wallets)
  end

  def create(conn, %{"wallet" => wallet_params, "country" => country_id}) do
    bag = Identity.get_bag!(country_id)
    with {:ok, %Wallet{} = wallet} <- Identity.create_wallet(wallet_params, bag) do
      conn
      |> put_status(:created)
      |> render("show.json", wallet: wallet)
    end
  end

  def show(conn, %{"id" => id}) do
    wallet = Identity.get_wallet!(id)
    render(conn, "show.json", wallet: wallet)
  end

  def update(conn, %{"id" => id, "wallet" => wallet_params}) do
    wallet = Identity.get_wallet!(id)
    #make sure fields address and code are not updated under any cirmumstance
    if Map.has_key?(wallet_params, "address") or Map.has_key?(wallet_params, "code") do
      send_resp(conn, :no_content, "")
    else
      with {:ok, %Wallet{} = wallet} <- Identity.update_wallet(wallet, wallet_params) do
        render(conn, "show.json", wallet: wallet)
      end
    end

  end

  def delete(conn, %{"id" => id}) do
    wallet = Identity.get_wallet!(id)
    Identity.delete_accounts_for_wallet(wallet.id)
    with {:ok, %Wallet{}} <- Identity.delete_wallet(wallet) do
      send_resp(conn, :no_content, "")
    end
  end
end
