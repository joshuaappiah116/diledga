defmodule DiledgaWeb.UserController do
  use DiledgaWeb, :controller

  alias Diledga.Identity
  alias Diledga.Identity.User

  action_fallback DiledgaWeb.FallbackController

  def index(conn, _params) do
    users = Identity.list_users()
    render(conn, "index.json", users: users)
  end

  def create(conn, %{"user" => user_params, "country" => country_id}) do
    #get the country the user can be found in
    bag = Identity.get_bag!(country_id)
    with {:ok, %User{} = user} <- Identity.create_user(user_params, bag) do
      conn
      |> put_status(:created)
      |> render("show.json", user: user)
    end
  end

  def show(conn, %{"id" => id}) do
    user = Identity.get_user!(id)
    render(conn, "show.json", user: user)
  end


  def delete(conn, %{"id" => id}) do
    user = Identity.get_user!(id)
    #delete all accounts that user has
    Identity.delete_accounts_for_user(user.id)
    with {:ok, %User{}} <- Identity.delete_user(user) do
      send_resp(conn, :no_content, "")
    end
  end
end
