defmodule DiledgaWeb.AccountController do
  use DiledgaWeb, :controller

  alias Diledga.Identity
  alias Diledga.Identity.Account

  action_fallback DiledgaWeb.FallbackController

  def index(conn, _params) do
    accounts = Identity.list_accounts()
    render(conn, "index.json", accounts: accounts)
  end


  def create(conn, %{"account" => account_params, "owner" => owner_id, "wallet" => wallet_id}) do
    #check if the passed parameters has address in there. if true return nothing and don't put
    #add it as a new account

    if Map.has_key?(account_params, "address") do
      send_resp(conn, :no_content, "")

    else
      #get the owner of the account
      owner = Identity.get_user!(owner_id)
      wallet = Identity.get_wallet!(wallet_id)

      #create the wallet address
      with {:ok, %Account{} = account} <- Identity.create_account(account_params, owner, wallet) do
        conn
        |> put_status(:created)
        |> render("show.json", account: account)
      end
    end

  end


  def show(conn, %{"id" => id}) do
    account = Identity.get_account!(id)
    render(conn, "show.json", account: account)
  end


  def update(conn, %{"id" => id, "account" => account_params}) do
    account = Identity.get_account!(id)

    if Map.has_key?(account_params, "address") do
      send_resp(conn, :no_content, "")
    else
      with {:ok, %Account{} = account} <- Identity.update_account(account, account_params) do
        render(conn, "show.json", account: account)
      end
    end
  end


  def delete(conn, %{"id" => id}) do
    account = Identity.get_account!(id)

    with {:ok, %Account{}} <- Identity.delete_account(account) do
      send_resp(conn, :no_content, "")
    end
  end


end
