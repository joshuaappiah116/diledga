defmodule DiledgaWeb.BagController do
  use DiledgaWeb, :controller

  alias Diledga.Identity
  alias Diledga.Identity.Bag

  action_fallback DiledgaWeb.FallbackController

  alias Diledga.Identity.CountryCode

  require Logger


  def index(conn, _params) do
    bags = Identity.list_bags()
    render(conn, "index.json", bags: bags)
  end


  def create(conn, %{"bag" => bag_params}) do
    #get the country code
    code =
      Map.get(bag_params, "code")
      |> CountryCode.get_country_code

    bag_params = Map.put(bag_params, "codedigit", code)
    with {:ok, %Bag{} = bag} <- Identity.create_bag(bag_params) do
      conn
      |> put_status(:created)
      |> render("show.json", bag: bag)
    end
  end


  def show(conn, %{"id" => id}) do
    bag = Identity.get_bag!(id)
    render(conn, "show.json", bag: bag)
  end


  def delete(conn, %{"id" => id}) do
    bag = Identity.get_bag!(id)

    with {:ok, %Bag{}} <- Identity.delete_bag(bag) do
      send_resp(conn, :no_content, "")
    end
  end
end
