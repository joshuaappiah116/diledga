defmodule DiledgaWeb.PageController do
  use DiledgaWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
