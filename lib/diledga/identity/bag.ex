defmodule Diledga.Identity.Bag do
  use Ecto.Schema
  import Ecto.Changeset

  #preferable system behaviour
  #change the unique index and let both name and code have their own separate indexes

  schema "bags" do
    field :code, :string
    field :name, :string
    field :codedigit, :integer
    has_many :wallets, Diledga.Identity.Wallet, on_delete: :delete_all
    has_many :users, Diledga.Identity.User, on_delete: :delete_all
    timestamps()
  end

  @doc false
  def changeset(bag, attrs) do
    bag
    |> cast(attrs, [:name, :code, :codedigit])
    |> validate_required([:name, :code, :codedigit])
    |> unique_constraint(:name, [name: :bags_name_code_index])
    |> unique_constraint(:codedigit, [name: :bags_codedigit_index])
    |> validate_length(:code, is: 2)
    |> validate_length(:name, max: 40)
  end
end
