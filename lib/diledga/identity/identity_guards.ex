defmodule Diledga.Identity.Guards do
  @moduledoc """
  Used for ensuring that issuied identities are of the right types
  """

  @doc """
  ensure that the given value is a private key
  """
  defguard is_privateKey(value) when tuple_size(value) == 5 and elem(value, 0) == :ECPrivateKey and is_tuple(elem(value, 3)) and elem(elem(value, 3), 0) == :namedCurve

  @doc """
  ensure that the given value is a public key
  """
  defguard is_publicKey(pub) when tuple_size(pub) == 2 and elem(elem(pub, 0), 0) == :ECPoint and is_tuple(elem(pub, 1)) and elem(elem(pub, 1), 0) == :namedCurve and tuple_size(pub) == 2

  @doc """
  ensure that the certificate is a self-signed
  """
  defguard is_certificate(value) when tuple_size(value) == 4 and elem(value, 0) == :OTPCertificate
end
