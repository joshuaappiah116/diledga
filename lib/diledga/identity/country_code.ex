defmodule Diledga.Identity.CountryCode do
  @moduledoc """
  generates the first 4 codes of the 8digits wallet code.
  The first 4 digits represent a country in which the wallet
  """
  #all 0's have been taken out of the list to ensure that any 0 that appears in
  #4 digits will be trailing 0 digits to make the 4 digits complete
  @codes %{
    "a" => 81,
    "b" => 82,
    "c" => 83,
    "d" => 84,
    "e" => 85,
    "f" => 86,
    "g" => 87,
    "h" => 88,
    "i" => 89,
    "j" => 71,
    "k" => 72,
    "l" => 73,
    "m" => 74,
    "n" => 75,
    "o" => 76,
    "p" => 77,
    "q" => 78,
    "r" => 79,
    "s" => 61,
    "t" => 62,
    "u" => 63,
    "v" => 64,
    "w" => 65,
    "x" => 66,
    "y" => 67,
    "z" => 68
  }

  @doc """
  get the code associated with a country
  """
  @spec get_country_code(binary()) :: integer()
  def get_country_code(code) do
    code =
      code
      |> String.downcase #convert to lowercase string
      |> String.graphemes #get each character into a list
      |> Enum.map(fn x -> Map.get(@codes, x) end)

    #get each value that comes with the system
    [h, t] = code
    newStr = Integer.to_string(h) <> Integer.to_string(t)

    String.to_integer(newStr)

  end

  @doc """
  get the country given the wallet code
  """
  @spec get_country(integer()) :: binary()
  def get_country(code) do
    #split the digits
    [f1, f2, s1, s2] = Integer.digits(code)
    first =
      Integer.to_string(f1) <> Integer.to_string(f2)
      |> String.to_integer

    second =
      Integer.to_string(s1) <> Integer.to_string(s2)
      |> String.to_integer

    #get the country that matches each code
    {c1, _} = Enum.find(@codes, fn {_x, y} -> y == first end)
    {c2, _} = Enum.find(@codes, fn {_x, y} -> y == second end)

    String.upcase(c1 <> c2)
  end
end
