defmodule Diledga.Authority do
  @moduledoc """
    responsible for creating a certificate authority. The files must be copied and
    The files must be created and distributed manually
  """

  @name Identity.Certificate.Authority
  @cert_name "ca_cert.der"
  @priv_key "ca_key.key"
  @serial 97195003

  require Logger

  @doc """
  creates a certificate authority and deposits the files into a folder.
  In distributed mode, please make sure only one node creates this key and certificate.
  And distribute the values to other nodes manually
  """
  @spec create_ca() :: atom() | tuple()
  def create_ca() do
    #get the configuration settings passed to the server with name Identity.Certificate.Authority
    auth = Application.get_env(:diledga, @name)
    #get the directory where the digital information can be found
    home_dir = auth[:home_dir]
    certificate = Path.join(home_dir, auth[:auth_dir])
    pri_key = Path.join(home_dir, auth[:priv_key])

    #we create a new certificate for the system
    sequence = auth[:rdn] #rdn sequence that uniquely id
    secret = auth[:secret] #supplied to extension as subject alternative name and must be kept secret.
    days = auth[:best_before]


    #check if certificate folder exist already
    if !File.dir?(certificate) do
      File.mkdir_p!(certificate)
    end

    #check if key folder exists
    if !File.dir?(pri_key) do
      File.mkdir_p!(pri_key)
    end

    if sequence == nil or secret == nil or days == nil do
      Logger.error("rdn sequence, secret and best before required for creation of certificate authority...")
      {:error, :sequence_secret_days_required}
    else
      #get the filenames for certificate and key
      cert_file = Path.join(certificate, @cert_name)
      key_file = Path.join(pri_key, @priv_key)


      secret_encode =
        :crypto.hash(:sha256, secret)
        |> Base.hex_encode32(padding: false, case: :lower)


      #create a new certificate and key as the certificate authority
      ca_key = X509.PrivateKey.new_ec(:secp256r1)

      ca_auth = X509.Certificate.self_signed(ca_key,
                sequence,
                [template: :root_ca, extensions: [
                            subject_alt_name: X509.Certificate.Extension.subject_alt_name([secret_encode])
                          ],
                  validity: X509.Certificate.Validity.days_from_now(days),
                  serial: @serial
                ])

      #convert the key and cert into a der format

      key = X509.PrivateKey.to_der(ca_key)
      cert = X509.Certificate.to_der(ca_auth)

      #write them to file
      File.write!(key_file, key, [:binary])
      File.write!(cert_file, cert, [:binary])

      #return okay if everything runs smoothly
      Logger.info("Key and certificate written to file...")
      :ok
    end

  end

end
