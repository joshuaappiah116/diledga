defmodule Diledga.Identity.Account do
  use Ecto.Schema
  import Ecto.Changeset

  schema "accounts" do
    field :address, :string
    field :name, :string
    belongs_to :wallet, Diledga.Identity.Wallet
    belongs_to :user, Diledga.Identity.User

    timestamps()
  end

  @doc false
  def changeset(account, attrs) do
    account
    |> cast(attrs, [:name, :address])
    |> validate_required([:name, :address])
    |> unique_constraint(:name, name: :accounts_name_wallet_id_index)
    |> unique_constraint(:address, name: :accounts_address_index)
  end
end
