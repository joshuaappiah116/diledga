defmodule Diledga.Oracle.Address do
  @moduledoc """
  takes the address and verifies the address for authenticity.
  These are the following activities the address perform

  1. It takes a new account and create an address for it
  2. It takes a wallet and create an address for it
  3. It takes an account/wallet domain and returns the address tied to it
  4. It takes an account domain/wallet and returns a boolean as to if the account is valid
  6. It takes an address and it's owner and verify if the owner realy owns it
  """

  @algo :sha256
  @userVersion <<0x01>>
  @walletVersion <<0x02>>

  require Logger

  alias Diledga.Identity

  @doc """
  create a new account given the account details.
  Controlling oracle should make sure the provided bag and wallet are all existing
  """
  @spec new_account_address(map()) :: binary() | tuple()
  def new_account_address(map) do
    if Map.has_key?(map, "name") and Map.has_key?(map, "bag") and Map.has_key?(map, "wallet") and Map.has_key?(map, "publicKey")do
      account_address(map)
    else
      {:error, :incomplete_account_details}
    end
  end


  @doc """
  creates an address for a wallet in the system
  """
  @spec new_wallet_address(map()) :: binary() | tuple()
  def new_wallet_address(map) do
    if Map.has_key?(map, "bag") and Map.has_key?(map, "wallet") and Map.has_key?(map, "publicKey") do
      wallet_address(map)
    else
      {:error, :incomplete_wallet_details}
    end
  end


  @doc """
  takes an account domain name and returns the account

  account domain is given as eg. user123.mtngh.gh.lud
  """
  @spec get_acccount_address(binary(), binary()) :: binary() | tuple()
  def get_acccount_address(domain, pubkey) do
    account_domain(domain, pubkey)
  end


  @doc """
  takes a wallet domain name and returns the corresponding account

  wallet domain is given as eg. mtngh.gh.lud
  """
  @spec get_wallet_address(binary(), binary()) :: binary() | tuple()
  def get_wallet_address(domain, pubkey) do
    wallet_domain(domain, pubkey)
  end


  #============================================================================
  #helper functions
  #============================================================================

  defp wallet_domain(domain, pubkey) do
    #split the domain and verify if they contain exactly what is required
    domain = String.split(domain, ".")
    if List.last(domain) == "lud" do
      if length(domain) == 3 do
        {wallet, _} = List.pop_at(domain, 0)
        {bag, _} = List.pop_at(domain, 1)

        #get the country associated with bag
        country = Identity.get_bag_by_code!(String.upcase(bag))
        #get the wallet by it's public name
        wal = Identity.get_wallet_by_publc_name!(wallet, country.id)

        #find the appropriate
        wallet_address(%{"bag" => bag, "wallet" => wal.code, "publicKey" => pubkey})
      else
        {:error, :unmatching_account_domain}
      end

    else
      {:error, :unknown_wallet_domain}
    end
  end


  defp account_domain(domain, pubkey) do
    #split the domain and verify if they contain exactly what is required
    domain = String.split(domain, ".")
    if List.last(domain) == "lud" do
      if length(domain) == 4 do
        #perform the addressing process
        {name, _} = List.pop_at(domain, 0)
        {wallet, _} = List.pop_at(domain, 1)
        {bag, _} = List.pop_at(domain, 2)

        #get the country associated with bag
        country = Identity.get_bag_by_code!(String.upcase(bag))
        #get the wallet by it's public name
        wal = Identity.get_wallet_by_publc_name!(wallet, country.id)
        #get account by name
        Identity.get_account_by_name!(name, wal.id)

        account_address(%{"bag" => bag, "wallet" => wal.code, "name" => name, "publicKey" => pubkey})

      else
        {:error, :unmatching_account_domain}
      end

    else
      {:error, :unknown_account_domain}
    end
  end

  @spec wallet_address(map()) :: binary()
  defp wallet_address(map) do
    #ensure the the bag and wallet exists in the database
    bag =
      Map.get(map, "bag") #a binary which is two letter long
      |> String.downcase
    wallet = Map.get(map, "wallet") #a wallet identifier which is 8 digits long
    pubkey = Map.get(map, "publicKey") #account's accompanying public key
    #concatenate all of them
    details = bag <> <<wallet>> <> pubkey
    hash1 = :crypto.hash(@algo, details)
    #add user version hash
    hash2 = :crypto.hash(@algo, @walletVersion <> hash1)

    encoded =
      :crypto.hash(:ripemd160, hash2) #perform ripemd160 to reduce the size to 20bytes
      |> B58.encode58 #compute the base58 of the binary algorithm

    #add the country to the first letter of the address
    String.upcase(bag) <> encoded
  end


  @spec account_address(map()) :: binary()
  defp account_address(map) do
    #ensure the the bag and wallet exists in the database
    bag = Map.get(map, "bag") #a binary which is two letter long
    wallet = Map.get(map, "wallet") #a wallet identifier which is 8 digits long
    name = Map.get(map, "name") #a unique binary name
    pubkey = Map.get(map, "publicKey") #account's accompanying public key
    #concatenate all of them
    details = String.downcase(bag) <> <<wallet>> <> name <> pubkey
    hash1 = :crypto.hash(@algo, details)
    #add user version hash
    hash2 = :crypto.hash(@algo, @userVersion <> hash1)

    encoded =
      :crypto.hash(:ripemd160, hash2) #perform ripemd160 to reduce the size to 20bytes
      |> B58.encode58 #compute the base58 of the binary algorithm

    #add the country to the first letter of the address
    String.upcase(bag) <> encoded
  end

end
