defmodule Diledga.Identity.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :name, :string
    belongs_to :bag, Diledga.Identity.Bag
    has_many :accounts, Diledga.Identity.Account, on_delete: :delete_all

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> unique_constraint(:name, name: :users_name_index)
  end
end
