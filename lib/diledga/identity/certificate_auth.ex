defmodule Diledga.Identity.CertificateAuthority do
  @moduledoc """
  Responsible for creating the certificate authority required in the system for
  ensuring each user present in the system was issued by the system and not some
  malicious entity
  """
  use GenServer

  alias X509.Certificate

  @name Identity.Certificate.Authority
  @cert_name "ca_cert.der"
  @priv_key "ca_key.key"

  require Logger
  #@serial 97195003

  @doc """
  starts the CA server
  """
  @spec start_link(any()) :: {atom(), any()}
  def start_link(_args) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  

  @doc """
  issues a new certificate for a public key
  """


  def init(_args) do
    #get the configuration settings passed to the server with name Identity.Certificate.Authority
    auth = Application.get_env(:diledga, @name)
    #get the directory where the digital information can be found
    home_dir = auth[:home_dir]
    certificate = Path.join([home_dir, auth[:auth_dir], @cert_name])
    pri_key = Path.join([home_dir, auth[:priv_key], @priv_key])

    #check if the files exists
    if !File.exists?(certificate) or !File.exists?(pri_key) do
      Logger.error("Certificate files and Private key files are not known")
      {:stop, :certificate_and_privatekey_missing}
    else
      #get the binary into memory
      cert =
        File.read!(certificate)
        |> X509.Certificate.from_der!


      priv =
        File.read!(pri_key)
        |> X509.PrivateKey.from_der!

      #get the public key associated with the certificate
      pub_key = X509.Certificate.public_key(cert)
      {:ok, %{ca_key: priv, ca_cert: cert, ca_pub: pub_key}}
    end
  end

end
