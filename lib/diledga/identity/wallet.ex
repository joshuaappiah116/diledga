defmodule Diledga.Identity.Wallet do
  use Ecto.Schema
  import Ecto.Changeset

  #code should and bag must be unique in the system
  schema "wallets" do
    field :address, :string
    field :code, :integer
    field :name, :string
    field :public_name, :string
    belongs_to :bag, Diledga.Identity.Bag
    has_many :accounts, Diledga.Identity.Account, on_delete: :delete_all

    timestamps()
  end

  @doc false
  def changeset(wallet, attrs) do
    wallet
    |> cast(attrs, [:name, :public_name, :address, :code])
    |> validate_required([:name, :public_name, :address, :code])
    |> unique_constraint(:public_name, name: :wallets_bag_id_public_name_index)
    |> unique_constraint(:code, name: :wallets_code_index)
    |> unique_constraint(:address, name: :wallets_address_index)
  end
end
