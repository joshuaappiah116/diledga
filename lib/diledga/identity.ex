defmodule Diledga.Identity do
  @moduledoc """
  The Identity context.
  """

  import Ecto.Query, warn: false
  alias Diledga.Repo

  alias Diledga.Identity.Bag

  @doc """
  Returns the list of bags.

  ## Examples

      iex> list_bags()
      [%Bag{}, ...]

  """
  def list_bags do
    Repo.all(Bag)
  end

  @doc """
  Gets a single bag.

  Raises `Ecto.NoResultsError` if the Bag does not exist.

  ## Examples

      iex> get_bag!(123)
      %Bag{}

      iex> get_bag!(456)
      ** (Ecto.NoResultsError)

  """
  def get_bag!(id), do: Repo.get!(Bag, id)


  @doc """
  get bag by code
  """
  def get_bag_by_code!(code), do: Repo.get_by!(Bag, code: code)

  @doc """
  Creates a bag.

  ## Examples

      iex> create_bag(%{field: value})
      {:ok, %Bag{}}

      iex> create_bag(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_bag(attrs \\ %{}) do
    %Bag{}
    |> Bag.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a bag.

  ## Examples

      iex> update_bag(bag, %{field: new_value})
      {:ok, %Bag{}}

      iex> update_bag(bag, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_bag(%Bag{} = bag, attrs) do
    bag
    |> Bag.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Bag.

  ## Examples

      iex> delete_bag(bag)
      {:ok, %Bag{}}

      iex> delete_bag(bag)
      {:error, %Ecto.Changeset{}}

  """
  def delete_bag(%Bag{} = bag) do
    Repo.delete(bag)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking bag changes.

  ## Examples

      iex> change_bag(bag)
      %Ecto.Changeset{source: %Bag{}}

  """
  def change_bag(%Bag{} = bag) do
    Bag.changeset(bag, %{})
  end

  alias Diledga.Identity.Wallet

  @doc """
  Returns the list of wallets.

  ## Examples

      iex> list_wallets()
      [%Wallet{}, ...]

  """
  def list_wallets do
    Repo.all(Wallet)
  end

  @doc """
  Gets a single wallet.

  Raises `Ecto.NoResultsError` if the Wallet does not exist.

  ## Examples

      iex> get_wallet!(123)
      %Wallet{}

      iex> get_wallet!(456)
      ** (Ecto.NoResultsError)

  """
  def get_wallet!(id), do: Repo.get!(Wallet, id)

  @doc """
  get wallet by public name
  """
  def get_wallet_by_publc_name!(name, bag) do
    query =
      from w in Wallet,
      where: w.public_name == ^name and w.bag_id == ^bag
    Repo.one!(query)

  end

  @doc """
  Creates a wallet.

  ## Examples

      iex> create_wallet(%{field: value})
      {:ok, %Wallet{}}

      iex> create_wallet(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_wallet(attrs \\ %{}, bag) do
    Ecto.build_assoc(bag, :wallets)
    |> Wallet.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a wallet.

  ## Examples

      iex> update_wallet(wallet, %{field: new_value})
      {:ok, %Wallet{}}

      iex> update_wallet(wallet, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_wallet(%Wallet{} = wallet, attrs) do
    wallet
    |> Wallet.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Wallet.

  ## Examples

      iex> delete_wallet(wallet)
      {:ok, %Wallet{}}

      iex> delete_wallet(wallet)
      {:error, %Ecto.Changeset{}}

  """
  def delete_wallet(%Wallet{} = wallet) do
    Repo.delete(wallet)
  end


  @doc """
  delete all wallets that are registered under country
  """
  def delete_wallets_of_country(id) do
    from(w in Wallet, where: w.bag_id == ^id)
    |> Repo.delete_all
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking wallet changes.

  ## Examples

      iex> change_wallet(wallet)
      %Ecto.Changeset{source: %Wallet{}}

  """
  def change_wallet(%Wallet{} = wallet) do
    Wallet.changeset(wallet, %{})
  end

  alias Diledga.Identity.User

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}, bag) do
    Ecto.build_assoc(bag, :users)
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end


  @doc """
  delete all users registered as members of a country
  """
  def delete_users_of_country(id) do
    from(u in User, where: u.bag_id == ^id)
    |> Repo.delete_all
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end

  alias Diledga.Identity.Account

  @doc """
  Returns the list of accounts.

  ## Examples

      iex> list_accounts()
      [%Account{}, ...]

  """
  def list_accounts do
    Repo.all(Account)
  end

  @doc """
  Gets a single account.

  Raises `Ecto.NoResultsError` if the Account does not exist.

  ## Examples

      iex> get_account!(123)
      %Account{}

      iex> get_account!(456)
      ** (Ecto.NoResultsError)

  """
  def get_account!(id), do: Repo.get!(Account, id)


  @doc """
  get account by name
  """
  def get_account_by_name!(name, wallet) do
    query =
      from a in Account,
      where: a.name == ^name and a.wallet_id == ^wallet

    Repo.one!(query)
  end

  @doc """
  Creates a account.

  ## Examples

      iex> create_account(%{field: value})
      {:ok, %Account{}}

      iex> create_account(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_account(attrs \\ %{}, user, wallet) do
    a1 = Ecto.build_assoc(user, :accounts)

    Ecto.build_assoc(wallet, :accounts, a1)
    |> Account.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a account.

  ## Examples

      iex> update_account(account, %{field: new_value})
      {:ok, %Account{}}

      iex> update_account(account, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_account(%Account{} = account, attrs) do
    account
    |> Account.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Account.

  ## Examples

      iex> delete_account(account)
      {:ok, %Account{}}

      iex> delete_account(account)
      {:error, %Ecto.Changeset{}}

  """
  def delete_account(%Account{} = account) do
    Repo.delete(account)
  end


  @doc """
  delete all accounts that match a user's id
  """
  def delete_accounts_for_user(id) do
    from(a in Account, where: a.user_id == ^id)
    |> Repo.delete_all
  end


  @doc """
  delete all accounts that matches an wallet id
  """
  def delete_accounts_for_wallet(id) do
    from(a in Account, where: a.walle_id == ^id)
    |> Repo.delete_all
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking account changes.

  ## Examples

      iex> change_account(account)
      %Ecto.Changeset{source: %Account{}}

  """
  def change_account(%Account{} = account) do
    Account.changeset(account, %{})
  end
end
