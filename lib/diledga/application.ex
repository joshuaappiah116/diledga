defmodule Diledga.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start the Ecto repository
      Diledga.Repo,
      # Start the endpoint when the application starts
      DiledgaWeb.Endpoint,
      {Registry, keys: :unique, name: Transaction.LockRegistry},

      #starts the the access lock server
      Diledga.Transaction.AccessLock,

      #starts the output in memory
      Diledga.Transaction.OutputInMemory,

      #starts the input in memory stream
      Diledga.Transaction.InputInMemory,

      #starts the disk logger
      Diledga.Audit.DiskLogger
      # Starts a worker by calling: Diledga.Worker.start_link(arg)
      # {Diledga.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Diledga.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    DiledgaWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
