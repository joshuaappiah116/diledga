defmodule Diledga.Audit.DiskLogger do
  @moduledoc """
  logs the errors to a disk file using erlang's disk_log module
  """

  use GenServer

  require Logger

  @name :disklogs
  @size 10_000_000
  @max_file 10

  @doc """
  starts the logging server
  """
  @spec start_link(any()) :: tuple()
  def start_link(_args) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  @doc """
  receives a message and logs it to file asynchronously
  """
  @spec log_to_file(binary()) :: atom()
  def log_to_file(log) do
    GenServer.cast(__MODULE__, {:log, log})
  end


  @doc """
  read docs from disk to console
  """
  @spec read_logs(integer()) :: binary()
  def read_logs(size \\ 64_000) do
    GenServer.call(__MODULE__, {:read, size})
  end


  @doc """
  stops the logs reader
  """
  def close_logger() do
    GenServer.cast(__MODULE__, :close)
  end


  def init(_any) do
    #check if the logs directory exists already, if not create the directory
    if !File.dir?("logs") do
      File.mkdir!("logs")
    end

    #get the details of the file
    opts = Application.get_env(:logger, :disk_log_backend)
    #file_max = Application.get_env(:logger, :disk_log_backend)[:max_no_files]

    size = Keyword.get(opts, :max_file_size, @size)
    file_max = Keyword.get(opts, :max_no_files, @max_file)
    disk_opts = [name: @name, linkto: self(), type: :wrap, size: {size, file_max}, notify: true, repair: true, head: "Error and Warning logs", file: 'logs/']
    #disk_opts = [{:name, @name}, {:file, file}, {:linkto, self()}, {:type, :wrap}, {:size, {size, file_max}}, {:notify, true}, {:repair, true}, {:head, "Error and Warning logs"}]
    #start the disk logger
    case :disk_log.open(disk_opts) do
      {:ok, log} ->
        {:ok, %{log: log, chunk: :start, step: 1}}
      {:repaired, log, _, _} ->
        {:ok, %{log: log, chunk: :start}}
      {:error, reason} ->
        {:stop, :error_starting_disk_logger, reason}
    end
  end


  def handle_cast({:log, log}, %{log: logFile} = state) do
    :disk_log.alog(logFile, log)
    {:noreply, state}
  end

  def handle_cast(:close, %{log: log} = state) do
    :disk_log.close(log)
    {:noreply, state}
  end


  def handle_call({:read, size}, _f, %{log: log, chunk: chunk, step: step} = state) do
    {cont, term} = :disk_log.chunk(log, chunk, size)
    :disk_log.chunk_step(log, chunk, step)
    {:reply, term, %{state | chunk: cont, step: step + 1}}
  end

  def handle_info({:disk_log, _node, _log, info}, state) do
    Logger.info("We received this information: #{inspect info}")
    {:noreply, state}
  end
end
