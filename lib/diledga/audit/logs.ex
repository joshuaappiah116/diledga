defmodule Diledga.Audit.DiskLogBackend do
  @moduledoc """
  this module logs internal information onto disk.
  It logs all 4 levels to file with differing logging style but yet
  a default logging style
  """

  @behaviour :gen_event
  @name :disk_log_backend

  require Logger

  @format_default "$date ; $time, $metadata [$level] $message"


  @doc """
  starts a new LogBackend and get the default values
  """
  def init(_args) do
    #get the supplied configurations for :disk_log_backend
    config =
      Application.get_env(:logger, @name)
      |> Enum.into(%{})
    {:ok, config}
  end


  @doc """
  takes runtime configurations and merges it with the existing details.
  The runtime configuration will take precidence after the merge
  """
  def handle_event({:config, opts}, state) do
    config = Enum.into(opts, %{})
    {:ok, Map.merge(state, config)}
  end

  @doc """
  for flushing any remaining data out. Will not be used
  """
  def handle_event(:flush, state) do
    {:ok, state}
  end


  @doc """
  handles incoming logs.
  This will push the logs into file and rotate the files over 10 log files with
  a maximum of 1gig file size for each file. Hence we will be keeping up to 10gig
  of at a time. After reaching the 10gig mark, old files will be deleted by rota
  tion of the files
  """
  def handle_event({level, _group_leader, {Logger, message, timestamp, metadata}}, state) do
    #we get the minimum level
    minLevel = Map.get(state, :level, :debug)

    #compare the levels
    if Logger.compare_levels(level, minLevel) == :gt or Logger.compare_levels(level, minLevel) == :eq do
      #get the metadata required
      metaInfo = Map.get(state, :metadata, [])
      meta =
        if metaInfo == :all do
          metadata
        else
          Enum.filter(metadata, fn {x, _y} -> x in metaInfo end)
        end

      #get the format else use the default format and compile and make ready for input
      format =
        Map.get(state, :format, @format_default)
        |> Logger.Formatter.compile
        |> Logger.Formatter.format(minLevel, message, timestamp, meta)
        |> IO.chardata_to_string

        Diledga.Audit.DiskLogger.log_to_file(format)
      {:ok, state}

    else
      {:ok, state}
    end

  end


  def handle_call(_, state) do
    {:ok, :result, state}
  end

end
