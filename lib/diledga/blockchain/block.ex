defmodule Diledga.Blockchain.Block do
  @moduledoc """
  struct that defines the structure of blocks in the blockchain.
  Each block has a struct that has the following fields;
  1. blocksize - determines the size of the whole blockchain network
  2. blokheader - a metadat that summaries the current block
  3. transactionCounter - counts the total number of transactions in the block
  4. transactions - a list of encoded transactions in the blockchain
  """

  alias Diledga.Blockchain.BlockHeader
  alias Diledga.Blockchain.Block

  #types definition
  @type error :: {atom(), atom()}
  #@type string :: String.t


  @derive Jason.Encoder
  defstruct [blocksize: 0, blockheader: nil, transCounter: nil, transactions: nil]


  @doc """
  convert the string maps to atom maps
  """
  @spec to_atom_map(map()) :: map()
  def to_atom_map(block) do
    %Block{
      blocksize: block["blocksize"],
      blockheader: BlockHeader.to_atom_map(block["blockheader"]),
      transCounter: block["transCounter"],
      transactions: block["transactions"]
    }
  end

  @doc """
  takes the block header and the block and adds the block header to the block
  data.
  """
  @spec set_block_header(map(), map()) :: map()
  def set_block_header(block = %Block{}, header = %BlockHeader{}) do
    %Block{block | blockheader: header}
  end


  @doc """
  takes the block transactions list and the block and adds the block to the data
  """
  @spec set_transactions(map(), list()) :: map()
  def set_transactions(block  = %Block{}, transactions) when is_list(transactions) do
    %Block{block | transactions: transactions, transCounter: length(transactions)}
  end


  @doc """
  takes all the parts of the transaction and calculates the size of the block.
  """
  @spec set_blocksize(map()) :: map() | error
  def set_blocksize(block = %Block{})  do
    #check if all fields are present
    if block.blockheader == nil or block.transCounter == nil or block.transactions == nil do
      {:error, :incomplete_block}
    else
      #encode and get the encoded size.
      encode = Jason.encode!(block)
      size = byte_size(encode)
      #convert the size to binary
      size_bytes = byte_size(<<size>>)
      #subtract the 1 byte (0) from the size and add size_bytes to it
      size = size - 1 + size_bytes

      #set the new size as the size of the of the block
      %Block{block | blocksize: size}
    end
  end


  @doc """
  encodes the blocks for transportation over the network
  """
  @spec encode_block(map()) :: binary()
  def encode_block(block = %Block{}) do
    Jason.encode!(block)
  end

  @doc """
  decodes encoded blocks and makes available for processing
  """
  @spec decode_block(binary()) :: map()
  def decode_block(block) do
    Jason.decode!(block)
  end



end
