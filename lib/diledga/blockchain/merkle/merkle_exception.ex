defmodule Diledga.MerkleException do
  defexception(message: "Empty hash list. Can't create a merkle tree", can_retry: true)
end
