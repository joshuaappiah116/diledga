defmodule Diledga.Merkle do
  @moduledoc """
  We perform the following the whole merkle operations in this module
  """

  alias Diledga.Merkle.Node, as: MerkleNode
  @hash :sha256

  require Logger

  @doc """
  generates a merkle tree with all nodes present
  """
  @spec merkle_tree(list()) :: {[map()], map()}
  def merkle_tree(nodes) when is_list(nodes) do
    cond do
      length(nodes) == 0 ->
        #return empty list
        raise Diledga.MerkleException

      length(nodes) == 1 ->
        [h | _t] = nodes
        #return the merkle
        {[%MerkleNode{self: h}], %MerkleNode{self: h}}

      length(nodes) > 1 ->
        nodes = check_binary_tree_compliance(Enum.map(nodes, fn node -> %MerkleNode{self: node} end))
        run_algo(Enum.chunk_every(nodes, 2), List.first(nodes).height, [])

    end
  end


  @doc """
  proof that the given hash forms part of the merkle root presented
  """
  @spec merkle_proof(list(), binary(), binary()) :: boolean()
  def merkle_proof(nodes, roothash, hash) do
    #check if the root node matches our roothash
    root = List.last(nodes)
    if root.self == roothash do
      #Logger.info("Roothash matches the given root hash")
      #find if the hash is in the nodes
      run_proof(nodes, roothash, hash, 1, root.height)
    else
      false
    end

  end


  defp run_proof(_nodes, roothash, hash, cHeight, fHeight) when cHeight - 1 == fHeight do
    #check if the hash equals roothash
    if roothash == hash do
      true

    else
      false
    end
  end

  defp run_proof(nodes, roothash, hash, currentHeight, finalHeight) do
    node = Enum.find(nodes, fn x -> x.height == currentHeight and (x.leftnode == hash or x.rightnode == hash) end)
    if node == nil do
      false
    else
      #get the self of the node
      run_proof(nodes, roothash, node.self, currentHeight + 1, finalHeight)
    end
  end



  #helper functions
  #it takes a list of new nodes and a list of nodes that has already formed the merkle tree
  defp check_binary_tree_compliance(nodes) do
    #check if the remender of dividing nodes by two is zero or not
    if rem(length(nodes), 2) == 0 do
      nodes
    else
      #the list is odd and hence cannot form a binary tree
      #get the last hash and duplicate it.
      last = List.last(nodes)
      List.insert_at(nodes, -1, last)
    end
  end




  defp run_algo(nodepairs, height, children), do: run_algo(nodepairs, height, children, [])

  defp run_algo([], _height, children, [root]), do: {List.insert_at(children, -1, root), root}

  defp run_algo([], _height, children, parent) do
    nodes = check_binary_tree_compliance(parent)
    run_algo(Enum.chunk_every(nodes, 2), List.first(nodes).height, children)
  end

  defp run_algo([nodepair | nodepairs], height, nodes, parent) do
    [left, right] = nodepair
    #concatenate and hash the nodes
    concat = left.self <> right.self
    nhash = :crypto.hash(@hash, concat)

    #store the information in the nodes
    newnode = %MerkleNode{self: nhash, leftnode: left.self, rightnode: right.self, height: height + 1}
    nodes =
      nodes
      |> List.insert_at(-1, left)
      |> List.insert_at(-1, right)

    run_algo(nodepairs, height, nodes, List.insert_at(parent, -1, newnode))
  end

end
