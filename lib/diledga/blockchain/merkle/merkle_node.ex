defmodule Diledga.Merkle.Node do

  @moduledoc """
  Use a node to define the merkle tree.
  we define a strut that takes
  self: the hash of the node itself
  leftnode: the left node that this node inherited
  rightnode: the right node that this node inherited
  height: the number of nodes beneath this current node.
  """

  defstruct [self: nil, leftnode: nil, rightnode: nil, height: 0]


end
