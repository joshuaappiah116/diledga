defmodule Diledga.Merkle.Crypto do
  @moduledoc """
  the cryptographic hash algorithm used by the network to hash transactions.
  """
  @algo :sha256


  @doc """
  uses sha256 cryptographic algorithm to hash all nodes in the network.
  The function is called by any node that wants to hash
  """
  def merkle_hash(input) do
    :crypto.hash(@algo, input)
  end
end
