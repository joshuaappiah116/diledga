defmodule Diledga.Repo do
  use Ecto.Repo,
    otp_app: :diledga,
    adapter: Ecto.Adapters.Postgres
end
