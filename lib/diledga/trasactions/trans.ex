defmodule Diledga.Transaction do
  @moduledoc """
  It implements the behaviour that enforces that all must be encoded and also
  decoded before they are transmitted over the network
  """

  @doc """
  callback that enforces encoding of the transaction
  """
  @callback encode(map()) :: binary()


  @doc """
  callback that enforces the decoding of the transaction on reception
  """
  @callback decode(binary()) :: map()


  @doc """
  callback the returns the name of the behaviour implementor
  """
  @callback owner_module() :: atom()
end
