defmodule Diledga.Transaction.Input do
  @moduledoc """
  This represents a transactions input information
  Every transaction input will have a number fields which include;
  1. txId = represents the current transaction id
  2. lstTxId = the id of the last input transaction of the current
  3. issuer = hashed issuer's public key
  4. asset = total pawa the issuer has in his wallet
  5. timestamp = time of the creation of this transaction
  """
  @behaviour Diledga.Transaction

  alias Diledga.Transaction.Input

  @derive Jason.Encoder
  defstruct [txId: nil, lstTxId: nil, issuer: nil, asset: nil, timestamp: nil]


  @doc """
  check if the input is set correct
  """
  @spec to_atom_map(map()) :: map()
  def to_atom_map(trans) do
    #check if all fields are set
    %Input{
      txId: trans["txId"],
      lstTxId: trans["lstTxId"],
      issuer: trans["issuer"],
      asset: trans["asset"],
      timestamp: trans["timestamp"]
    }
  end


  @doc """
  checks if the transaction is a valid one
  """
  @spec is_valid?(map()) :: boolean()
  def is_valid?(trans = %Input{}) do
    transaction = %{txId: trans.txId, lstTxId: trans.lstTxId, issuer: trans.issuer, asset: trans.asset, timestamp: trans.timestamp}
    with true <- Enum.any?(transaction, fn {_, x} -> x == nil end) do
      false
    else
      false ->
        true
    end
  end


  @doc """
  converts the map into a tuple
  """
  @spec to_tuple(map()) :: tuple()
  def to_tuple(trans = %Input{}) do
    {trans.txId, trans.lstTxId, trans.issuer, trans.asset, trans.timestamp}
  end

  @doc """
  set the previous generated transaction id
  """
  @spec set_lastId(map(), binary()) :: map()
  def set_lastId(transaction = %Input{}, lastId) do
    %Input{transaction | lstTxId: lastId}
  end


  @doc """
  set the current generated transaction id
  """
  @spec set_currentId(map(), binary()) :: map()
  def set_currentId(transaction = %Input{}, currentId) do
    %Input{transaction | txId: currentId}
  end


  @doc """
  set the issuer's public key
  """
  @spec set_issuer(map(), binary()) :: map()
  def set_issuer(transaction = %Input{}, issuer) do
    %Input{transaction | issuer: issuer}
  end


  @doc """
  set the total asset of the issuer
  """
  @spec set_asset(map(), float()) :: map()
  def set_asset(transaction = %Input{}, asset) do
    %Input{transaction | asset: asset}
  end


  @doc """
  set the total asset of the issuer
  """
  @spec set_timestamp(map()) :: map()
  def set_timestamp(transaction = %Input{}) do
    #get datetime of the block creation
    timestamp =
      DateTime.utc_now()
      |> DateTime.to_unix()
    %Input{transaction | timestamp: timestamp}
  end


  #behaviour implementations
  #git@gitlab.com:joshuaappiah116/diledga.git
  @doc """
  encodes the transactions
  """
  def encode(block = %Input{}) do
    "inp" <> Jason.encode!(block)
  end

  @doc """
  decodes transactions.
  Inherited from transaction module
  """
  def decode(block) do
    "inp" <> block = block
    Jason.decode!(block)
  end


  @doc """
  returns the name of the current module
  """
  def owner_module() do
    __MODULE__
  end


end
