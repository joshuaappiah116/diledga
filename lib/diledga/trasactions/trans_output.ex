defmodule Diledga.Transaction.Output do
  @moduledoc """
  This represents a transaction output information
  The fields held there in are;
  1. otxId - transaction id
  2. ownerTx - Owner transaction
  3. amount - transaction amount
  4. lockscript - signed hash of the recipient's public key
  5. receipient - hash of the recipient's public key (makes it easy to find outputs intended for a particular user)
  6. type - type of transaction output.
            completed - shows the transaction has ended.
            holding - shows the transaction is in a zombie state. i.e the timer has been stopped.
            transit - the transaction is being undertaken
            i.e all completed transactions are spendable transactions. Only completed transactions are spendable
  7. timestamp - time the output transaction was created (seconds from unix epoch)
  8. expiry time - a timestamp of the time the transaction will exipire. This
                   information is useful only in the transit state.
  """

  @behaviour Diledga.Transaction
  alias Diledga.Transaction.Output


  @derive Jason.Encoder
  defstruct [otxId: nil, ownerTx: nil, amount: nil, lockscript: nil, recipient: nil, type: nil, timestamp: nil, expiry: nil]



  @doc """
  check if the input is set correct
  """
  @spec to_atom_map(map()) :: map()
  def to_atom_map(trans) do
    #check if all fields are set
    %Output{
      otxId: trans["otxId"],
      ownerTx: trans["ownerTx"],
      type: trans["type"],
      amount: trans["amount"],
      timestamp: trans["timestamp"],
      lockscript: trans["lockscript"],
      recipient: trans["recipient"]
    }
  end


  @doc """
  checks if the transaction is a valid one
  """
  @spec is_valid?(map()) :: boolean()
  def is_valid?(trans = %Output{}) do
    transaction = %{otxId: trans.otxId, ownerTx: trans.ownerTx, amount: trans.amount, lockscript: trans.lockscript,
                    recipient: trans.recipient, type: trans.type, timestamp: trans.timestamp, expiry: trans.expiry}
    with true <- Enum.any?(transaction, fn {_, x} -> x == nil end) do
      false
    else
      false ->
        true
    end
  end


  @doc """
  converts the map into a tuple
  """
  @spec to_tuple(map()) :: tuple()
  def to_tuple(trans = %Output{}) do
    {trans.otxId, trans.ownerTx, trans.amount, trans.lockscript, trans.recipient, trans.type, trans.timestamp, trans.expiry}
  end




  @doc """
  set timestamp
  """
  @spec set_timestamp(map()) :: map()
  def set_timestamp(transaction = %Output{}) do
    #get datetime of the block creation
    timestamp =
      DateTime.utc_now()
      |> DateTime.to_unix()
    %Output{transaction | timestamp: timestamp}
  end



  @doc """
  set transaction expiry date
  """
  @spec set_expiry(map(), integer()) :: map()
  def set_expiry(transaction = %Output{}, timestamp) do
    %Output{transaction | expiry: timestamp}
  end



  @doc """
  set type
  """
  @spec set_type(map(), binary()) :: map()
  def set_type(transaction = %Output{}, type) when type == "completed" or type == "transit" or type == "holding" do
    %Output{transaction | type: type}
  end


  @doc """
  set recipient
  """
  @spec set_recipient(map(), binary()) :: map()
  def set_recipient(transaction = %Output{}, recipient) do
    %Output{transaction | recipient: recipient}
  end



  @doc """
  set lockscript
  """
  @spec set_lockscript(map(), binary()) :: map()
  def set_lockscript(transaction = %Output{}, lockscript) do
    %Output{transaction | lockscript: lockscript}
  end



  @doc """
  set transaction id
  """
  @spec set_otxId(map(), binary()) :: map()
  def set_otxId(transaction = %Output{}, otxId) do
    %Output{transaction | otxId: otxId}
  end

  @doc """
  set owning transaction input's transaction id
  """
  @spec set_ownerTx(map(), binary()) :: map()
  def set_ownerTx(transaction = %Output{}, ownerTx) do
    %Output{transaction | ownerTx: ownerTx}
  end

  @doc """
  set owning transaction input's transaction id
  """
  @spec set_amount(map(), integer()) :: map()
  def set_amount(transaction = %Output{}, amount) do
    %Output{transaction | amount: amount}
  end

  #behaviour implementations
  #git@gitlab.com:joshuaappiah116/diledga.git
  @doc """
  encodes the transactions
  """
  def encode(block = %Output{}) do

    "out" <> Jason.encode!(block)
  end

  @doc """
  decodes transactions.
  Inherited from transaction module
  """
  def decode(block) do
    "out" <> block = block
    Jason.decode!(block)
  end


  @doc """
  returns the name of the current module
  """
  def owner_module() do
    __MODULE__
  end


end
