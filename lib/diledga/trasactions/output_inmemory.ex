defmodule Diledga.Transaction.OutputInMemory do
  @moduledoc """
  this is an in memory ets table that stores all input transactions that has not
  been commited to the blockchain yet.
  """
  use GenServer

  @table :output_inmemory
  alias Diledga.Transaction.Output


    @doc """
    starts the transaction input database
    """
    @spec start_link(any()) :: tuple()
    def start_link(_any) do
      GenServer.start_link(__MODULE__, [], name: __MODULE__)
    end

    @doc """
    insert a new input transaction
    """
    @spec add_output_transaction(map()) :: tuple()
    def add_output_transaction(transaction = %Output{}) do
      if Output.is_valid?(transaction) do
        GenServer.cast(__MODULE__, {:outputTrans, transaction})
      else
        {:error, :nil_pointing}
      end

    end


    def init(_args) do
      :ets.new(@table, [:set, :protected, :named_table])
      {:ok, %{}}
    end


    def handle_cast({:outputTrans, transaction}, state) do
      #get the tuple form of the transaction
      tuple = Output.to_tuple(transaction)
      #insert into database
      :ets.insert(@table, tuple)
      {:noreply, state}
    end
end
