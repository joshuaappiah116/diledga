defmodule Diledga.Transaction.LockServerTest do
  require Logger
  @moduledoc """
  this is a testing module but will need to spawn multiple processes.
  Since it cannot handle this, we move it into the main library modules.
  """
  alias Diledga.Transaction.PubSub
  alias Diledga.Transaction.AccessLock

  def start() do
    #Logger.info("We are in the handle connection function!!!")
    account = "joshuaappiah116@gmail.com"
    AccessLock.listen(account, self())
    pids = PubSub.lookup(account)
    #Logger.info("the account state is: #{inspect account} \n")
    Logger.info("total number of registered nodes as at now are\n#{inspect length(pids)}")

    handle_connection()
  end

  def handle_connection() do
    account = "joshuaappiah116@gmail.com"
    nodes = PubSub.lookup(account)
    Logger.info("Processes: #{inspect nodes}\n\n\n")
    receive do
      {:ready_state, account} ->
        Logger.info("received: #{inspect {:ready_state, account}}")
        #send in your request to gain access
        case AccessLock.lock(account) do
          :locked ->
            Logger.info("I have access to the lock: #{inspect self()}")
          :listen ->
            Logger.info("unable to get access to the lock. Will try later!!!")
        end

        #we then output the total number of existing listening processes

        handle_connection()
    end
  end
end
