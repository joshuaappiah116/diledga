defmodule Diledga.Transaction.InputInMemory do
  @moduledoc """
  this is an in memory ets table that stores all input transactions that has not
  been commited to the blockchain yet.
  """
  use GenServer

  @table :input_inmemory
  alias Diledga.Transaction.Input


  @doc """
  starts the transaction input database
  """
  @spec start_link(any()) :: tuple()
  def start_link(_any) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  @doc """
  insert a new input transaction
  """
  @spec add_input_transaction(map()) :: tuple()
  def add_input_transaction(transaction = %Input{}) do
    if Input.is_valid?(transaction) do
      GenServer.cast(__MODULE__, {:inputTrans, transaction})
    else
      {:error, :nil_pointing}
    end

  end


  def init(_args) do
    :ets.new(@table, [:set, :protected, :named_table])
    {:ok, %{}}
  end


  def handle_cast({:inputTrans, transaction}, state) do
    #get the tuple form of the transaction
    tuple = Input.to_tuple(transaction)
    #insert into database
    :ets.insert(@table, tuple)
    {:noreply, state}
  end
end
