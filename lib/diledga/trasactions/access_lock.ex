defmodule Diledga.Transaction.AccessLock do
  @moduledoc """
  Locks down a transaction to avoid asynchronious transaction updates.
  Hence one process can update a single account at a time.
  """

  #NB: the calls can be converted into cast where on completion of the procees,
  #they send a message back to the caller using their pids. This way the server
  #does not lock until a response and hence capable of serving multiple people
  #at the same time.
  use GenServer

  alias Diledga.Transaction.PubSub

  require Logger

  @table :accesslock
  @timeout 2 * 1000


  #type definitions
  @type start_return :: {atom(), existance}
  @type existance :: pid() | atom() | tuple()

  @doc """
  starts a new gen server that starts and monitors the access lock ets table
  """
  @spec start_link(any()) :: start_return
  def start_link(_any) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end


  @doc """
  adds an account to the database and lock the account afterwards
  """
  @spec add_and_lock(binary()) :: atom()
  def add_and_lock(account) do
    GenServer.call(__MODULE__, {:add_and_lock, account})
  end

  @doc """
  remove an account from the in memory database
  """
  @spec remove(binary()) :: atom()
  def remove(account) do
    GenServer.cast(__MODULE__, {:remove, account})
  end


  @doc """
  checks if an account exists in the database
  """
  @spec lookup(binary()) :: list()
  def lookup(account) do
    GenServer.call(__MODULE__, {:lookup, account})
  end


  @doc """
  sends a lock request to lock the database
  """
  @spec lock(binary()) :: atom()
  def lock(account) do
    GenServer.call(__MODULE__, {:lock, account})
  end


  @doc """
  unlock the locked account so that others may gain access
  """
  @spec unlock(binary(), pid()) :: atom()
  def unlock(account, pid) when is_pid(pid) do
    GenServer.cast(__MODULE__, {:unlock, account, pid})
  end


  @doc """
  handle the process listening for unlocking calls
  """
  @spec listen(binary(), pid()) :: atom()
  def listen(account, pid) do
    GenServer.cast(__MODULE__, {:listen, account, pid})
  end



  def init(_args) do
    :ets.new(@table, [:set, :private, :named_table])
    {:ok, %{}}
  end


  def handle_call({:add_and_lock, account}, {pid, _value}, state) do
    case :ets.lookup(@table, account) do
      [] ->
        #set an internal timer and make it readily available
        timeref = Process.send_after(self(), {:timeout, account, pid}, @timeout)

        #insert the account and set its value to locked
        accounts = {account, "locked", pid, timeref}
        :ets.insert(@table, accounts)
        PubSub.register(account)
        {:reply, :locked, state}
      [_value] ->
        #return listen
        {:reply, :listen, state}
    end
  end


  def handle_call({:lookup, account}, _from, state) do
    {:reply, :ets.lookup(@table, account), state}
  end


  def handle_call({:lock, account}, {mpid, _value}, state) do
    #check the state of the account
    case :ets.lookup(@table, account) do
      [] ->
        {:reply, :non_existing, state}
      [{_, type, pid, _ref}] when type == "locked" and pid == mpid ->
        {:reply, :locked_owner, state}
      [{_, type, _pid, _ref}] when type == "locked" ->
        {:reply, :listen, state}
      [{_, type, _pid, _ref}] when type == "unlocked" ->

        #set an internal timer and make it readily available
        timeref = Process.send_after(self(), {:timeout, account, mpid}, @timeout)

        #set account to locked
        :ets.insert(@table, {account, "locked", mpid, timeref})
        {:reply, :locked, state}
    end
  end


  def handle_cast({:unlock, account, mpid}, state) do
    case :ets.lookup(@table, account) do
      [{_, type, pid, ref}] when type == "locked" and pid == mpid ->
        #set the value to unlocked
        Process.cancel_timer(ref)
        PubSub.remove_listener(account, pid)
        :ets.insert(@table, {account, "unlocked", nil, nil})
        PubSub.dispatch(account)
        {:noreply, state}
      _any ->
        {:noreply, state}
    end
  end


  def handle_cast({:remove, account}, state) do
    :ets.delete(@table, account)
    {:noreply, state}
  end

  def handle_cast({:listen, account, pid}, state) do
    PubSub.add_listener(account, pid)
    {:noreply, state}
  end


  def handle_info({:timeout, account, pid}, state) do
    #get the account and unlock it
    case :ets.lookup(@table, account) do
      [{_, type, _pid, ref}] when type == "locked" ->
        Process.cancel_timer(ref)
        :ets.insert(@table, {account, "unlocked", nil, nil})
        PubSub.remove_listener(account, pid)
        PubSub.dispatch(account)
        {:noreply, state}

      _any ->
        {:noreply, state}
    end
  end


end
