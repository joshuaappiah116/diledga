defmodule Diledga.Transaction.PubSub do
  @moduledoc """
  All transactions that do not have access to a particular process registers to
  this listener. When the lock is released, this listener will publish an unlocked
  message to all nodes. All nodes that receive the message will then try to gain
  the lock
  """

  alias Transaction.LockRegistry

  @doc """
  registers a new account
  """
  @spec register(binary()) :: tuple()
  def register(account) do
    Registry.register(LockRegistry, account, [])
  end

  @doc """
  unregisters a registered account
  """
  @spec unregister(binary()) :: atom()
  def unregister(account) do
    Registry.unregister(LockRegistry, account)
  end


  @doc """
  update the processes registering to listen for updates
  """
  @spec add_listener(binary(), pid()) :: tuple()
  def add_listener(account, process) do
    Registry.update_value(LockRegistry, account, fn list -> List.insert_at(list, -1, process) end)
  end


  @doc """
  remove a process from the list of registered processes
  """
  @spec remove_listener(binary(), pid()) :: tuple()
  def remove_listener(account, process) do
    Registry.update_value(LockRegistry, account, fn list ->
      if process in list do
        List.delete(list, process)
      else
        list
      end
  end)
  end


  @doc """
  lookup the number of processes awaiting for an account release broadcast
  """
  @spec lookup(binary()) :: list()
  def lookup(account) do
    case Registry.lookup(LockRegistry, account) do
      [] ->
        []
      [{_, list}] ->
        list
    end
  end


  @doc """
  dispatches information to all involved processes that might be interested in knowning
  the current state of the application
  """
  @spec dispatch(binary()) :: atom()
  def dispatch(account) do
    Registry.dispatch(LockRegistry, account, fn [{_pid, processes}] ->
      for pid <- processes do
        send(pid, {:ready_state, account})
      end
     end)
    :ok
  end
end
