defmodule Diledga.Repo.Migrations.CreateBags do
  use Ecto.Migration

  def change do
    create table(:bags) do
      add :name, :string, null: false
      add :code, :string, null: false, size: 2
      add :codedigit, :integer

      timestamps()
    end

    create unique_index("bags", [:name, :code])
    create unique_index("bags", [:codedigit])
  end
end
