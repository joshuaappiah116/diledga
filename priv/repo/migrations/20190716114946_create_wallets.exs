defmodule Diledga.Repo.Migrations.CreateWallets do
  use Ecto.Migration

  def change do
    create table(:wallets) do
      add :name, :string
      add :public_name, :string
      add :address, :string
      add :code, :integer
      add :bag_id, references(:bags, on_delete: :nothing)

      timestamps()
    end

    create unique_index(:wallets, [:bag_id, :public_name])
    create unique_index(:wallets, [:bag_id, :code])
    create unique_index(:wallets, [:address])
    create index(:wallets, [:bag_id])
  end
end
