defmodule Diledga.Repo.Migrations.CreateAccounts do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :name, :string
      add :address, :string
      add :wallet_id, references(:wallets, on_delete: :nothing)
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create unique_index(:accounts, [:name, :wallet_id])
    create unique_index(:accounts, [:address])
    create index(:accounts, [:wallet_id])
    create index(:accounts, [:user_id])
  end
end
