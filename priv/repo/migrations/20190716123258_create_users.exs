defmodule Diledga.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :name, :string
      add :bag_id, references(:bags, on_delete: :nothing)

      timestamps()
    end

    create unique_index(:users, [:name])
    create index(:users, [:bag_id])
  end
end
