# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Diledga.Repo.insert!(%Diledga.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Diledga.Oracle.Address
alias Diledga.Identity

require Logger

#Identity.create_bag(%{"name" => "Ghana", "code" => "GH", "codedigit" => 8788})

#get the country with id 1
#private_key = "61RG40810GG4D9D8S5S4UMJJUS04M8EA1HSU53JAN2VVNIKIL22KLKAD2026B7L01830GAK69373Q0O10UGK80Q20025L2T554Q1ACJ24GM0SDRF7R8JV2IJDSO1RPNNDL4SCC0OQ2QHSCP54P3K9B7TOB5VV7DAL3BVC2K1L2DGDV83T9HT9GEEJUH8IQ9QC4======"
public = "61CJ04O60SL8CI6E7K1021G85A34HJHT0C0GE0Q20025L2T554Q1ACJ24GM0SDRF7R8JV2IJDSO1RPNNDL4SCC0OQ2QHSCP54P3K9B7TOB5VV7DAL3BVC2K1L2DGDV83T9HT9GEEJUH8IQ9QC4======"

public =
  public
  |> Base.hex_decode32!

country = Identity.get_bag!(1)
addr = Address.new_wallet_address(%{"bag" => country.code, "wallet" => 22, "publicKey" => public})

Logger.info("The given wallet is: #{inspect addr} \n\n")
#wallet = Identity.create_wallet(%{"address" => addr, "code" => 0022, "name" => "mtn ghana", "public_name" => "mtngh"}, country)
#Logger.info("wallet is: #{inspect wallet}")
