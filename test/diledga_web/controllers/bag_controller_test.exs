defmodule DiledgaWeb.BagControllerTest do
  use DiledgaWeb.ConnCase

  alias Diledga.Identity
  alias Diledga.Identity.Bag

  require Logger

  @create_attrs %{
    code: "gh",
    name: "Ghana"
  }
  @update_attrs %{
    code: "ng",
    name: "nigeria"
  }
  @invalid_attrs %{code: nil, name: nil}

  def fixture(:bag) do
    {:ok, bag} = Identity.create_bag(@create_attrs)
    Logger.info("we are in bags: #{inspect bag}")
    bag
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all bags", %{conn: conn} do
      conn = get(conn, Routes.bag_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create bag" do
    test "renders bag when data is valid", %{conn: conn} do
      conn = post(conn, Routes.bag_path(conn, :create), %{"bag" => @create_attrs})
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.bag_path(conn, :show, id))

      Logger.info("created country is: #{inspect json_response(conn, 200)["data"]}")
    end


    #test "renders errors when data is invalid", %{conn: conn} do
    #  conn = post(conn, Routes.bag_path(conn, :create), %{"bag" => @invalid_attrs})
    #  assert json_response(conn, 422)["errors"] != %{}
    #end
  end


  describe "delete bag" do
    setup [:create_bag]

    test "deletes chosen bag", %{conn: conn, bag: bag} do
      conn = delete(conn, Routes.bag_path(conn, :delete, bag))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.bag_path(conn, :show, bag))
      end
    end
  end


  defp create_bag(_) do
    bag = fixture(:bag)
    {:ok, bag: bag}
  end
end

"""
describe "update bag" do
  setup [:create_bag]

  test "renders bag when data is valid", %{conn: conn, bag: %Bag{id: id} = bag} do
    conn = put(conn, Routes.bag_path(conn, :update, bag), bag: @update_attrs)
    assert %{"id" => ^id} = json_response(conn, 200)["data"]

    conn = get(conn, Routes.bag_path(conn, :show, id))

    assert %{
             "id" => id,
             "code" => "some updated code",
             "name" => "some updated name"
           } = json_response(conn, 200)["data"]
  end


  test "renders errors when data is invalid", %{conn: conn, bag: bag} do
    conn = put(conn, Routes.bag_path(conn, :update, bag), bag: @invalid_attrs)
    assert json_response(conn, 422)["errors"] != %{}
  end
end
"""
