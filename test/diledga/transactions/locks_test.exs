defmodule Diledga.Transaction.AccessLockTest do
  use ExUnit.Case

  alias Diledga.Transaction.AccessLock
  alias Diledga.Transaction.PubSub
  require Logger

  test "test_locks" do

    account = "joshuaappiah116@gmail.com"
    AccessLock.add_and_lock(account)
    #get the state of the account
    Logger.info("the account has been created: #{inspect account}")
    Logger.info("main thread is: #{inspect self()}")

    #spawn 10 children processes
    pids = for _pid <- 1..10  do
      pid = spawn(Diledga.Transaction.LockServerTest, :start, [])
    end

    Process.sleep(30000)

    account = "joshuaappiah116@gmail.com"
    nodes = PubSub.lookup(account)
    Logger.info("Finally the remaining process in the pid is: \n")
    Logger.info("Processes: #{inspect nodes}\n\n\n")
  end

  defp callself(n) when n > 0 do
    callself(n-1)
  end
end
