defmodule Diledga.Transaction.InputInmemoryTest do
  use ExUnit.Case
  alias Diledga.Transaction.InputInMemory
  alias Diledga.Transaction.Input
  alias Diledga.Transaction.Output
  alias Diledga.Transaction.OutputInMemory

  require Logger

  test "handle_inmemory_db", _contest do
    {_, pid} = InputInMemory.start_link()
    Logger.info("in memory server started successfully... #{inspect pid}")
    Logger.info("inserting input details into memory...")
    info = InputInMemory.add_input_transaction(%Input{})
    Logger.info("the insertion returned information: #{inspect info}")

  end


  test "handl_outinmemory_db", _context do
    Logger.info("\n\n")
    {_, pid} = OutputInMemory.start_link()
    Logger.info("in memory server started successfully... #{inspect pid}")
    Logger.info("inserting input details into memory...")
    info = OutputInMemory.add_output_transaction(%Output{otxId: "trans.otxId", ownerTx: "trans.ownerTx", amount: "trans.amount", lockscript: "trans.lockscript",
                    recipient: "trans.recipient", type: "trans.type", timestamp: "trans.timestamp", expiry: "trans.expiry"})
    Logger.info("the insertion returned information: #{inspect info}")
    Process.sleep(2000)
    value = :ets.lookup(:output_inmemory, "trans.otxId")
    Logger.info("output in memory information is: #{inspect value}")
  end


  #test "check input validity", _context do
    #input = %Input{}
  #  input = %Input{txId: "trans.txId", lstTxId: "trans.lstTxId", issuer: "trans.issuer", asset: "trans.asset", timestamp: "trans.timestamp"}
  #  valid? = Input.is_valid?(input)
  #  Logger.info("Is the input a valid one?: #{inspect valid?}")
  #  tuple = Input.to_tuple(input)
  #  Logger.info("Input has been changed into a tuple: #{inspect tuple}")
  #end
end
