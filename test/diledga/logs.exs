defmodule Diledga.LogsBackend do
  use ExUnit.Case

  require Logger

  test "blockheader_test", _context do

    #se will be sending 20000 error messages
    #Logger.info("We are solving account issues ", [user: "joshuaappiah116", id: 17])
    for x <- 1..20_000  do
      Logger.error("we have been receiving multiple errors with identifiers: #{inspect x} from user with id in meta", [user: "joshuaappiah", id: 20])
    end
    Logger.debug("Completed!!!")
    :ok
  end


end
