defmodule Diledga.AddressesTest do
  #use DiledgaWeb.ConnCase
  use ExUnit.Case
  require Logger

  alias Diledga.Oracle.Address
  alias Diledga.Identity

  @create_bag %{
    name: "Ghana",
    code: "GH",
    codedigit: 8788
  }

  setup do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Diledga.Repo)
  end

  setup_all do

    public = "61CJ04O60SL8CI6E7K1021G85A34HJHT0C0GE0Q20025L2T554Q1ACJ24GM0SDRF7R8JV2IJDSO1RPNNDL4SCC0OQ2QHSCP54P3K9B7TOB5VV7DAL3BVC2K1L2DGDV83T9HT9GEEJUH8IQ9QC4======"

    accpub = "61CJ04O60SL8CI6E7K1021G85A34HJHT0C0GE0Q20021QFO0JFU7KGJ4C11KENHTFC8OOE7TBVG62I8CO8I8KSSF6T6RBGK15EU7V9IBH47TCHHG99JMMNATS3KHDSVPUK31EFHCPBMMM35NE0======"
    on_exit(fn ->
      Diledga.Audit.DiskLogger.close_logger()
    end)

    {:ok, public: public, accpub: accpub}
  end


  describe "test_wallet_address"do
    test "test_wallet_addresses", %{public: public} = _context  do
      #create the bag
      {:ok, bag} = Identity.create_bag(@create_bag)
      Logger.info("Created a bag #{inspect bag}")
      #create the wallet
      addr = Address.new_wallet_address(%{"bag" => bag.code, "wallet" => 22, "publicKey" => Base.hex_decode32!(public)})

      {:ok, wallet} = Identity.create_wallet(%{"address" => addr, "code" => 22, "name" => "mtn ghana", "public_name" => "mtngh"}, bag)

      Logger.info("wallet is: #{inspect wallet}")

      #get the create a new address for this user
      address = "mtngh.gh.lud" #%{"bag" => "GH", "wallet" => 11000023}
      return = Address.get_wallet_address(address, Base.hex_decode32!(public))
      #return = Address.verify_wallet_address(address, public, "GHA7eQUMce77kChpkoXHo2TT3nott")

      Logger.info("returned wallet address: #{inspect return}")
      Logger.info("equality check: #{inspect return} == #{inspect wallet.address}")
      assert return == wallet.address
    end

  end


  describe "test_account_address" do
    test "test_accounts_addresses", %{public: public, accpub: accpub} = _context  do

      #create the bag
      {:ok, bag} = Identity.create_bag(@create_bag)
      #create the wallet
      addr = Address.new_wallet_address(%{"bag" => bag.code, "wallet" => 22, "publicKey" => Base.hex_decode32!(public)})

      {:ok, wallet} = Identity.create_wallet(%{"address" => addr, "code" => 22, "name" => "mtn ghana", "public_name" => "mtngh"}, bag)

      #create a new user
      {:ok, user} = Identity.create_user(%{name: "joshuaappiah116@gmail.com"}, bag)

      #create an account's address
      acddr = Address.new_account_address(%{"name" => "joshuaappiah116", "wallet" => wallet.code, "bag" => bag.code, "publicKey" => Base.hex_decode32!(accpub)})
      Logger.info("account address is: #{inspect acddr}")
      {:ok, account} = Identity.create_account(%{address: acddr, name: "joshuaappiah116"}, user, wallet)

      #get the create a new address for this user
      address = "joshuaappiah116.mtngh.gh.lud" #%{"bag" => "GH", "wallet" => 11000023}
      return = Address.get_acccount_address(address, Base.hex_decode32!(accpub))
      #return = Address.verify_wallet_address(address, public, "GHA7eQUMce77kChpkoXHo2TT3nott")

      Logger.info("returned wallet address: #{inspect return}")
      Logger.info("equality check: #{inspect return} == #{inspect account.address}")
      assert return == account.address

    end

  end

  #test "create_account_address", %{account: public} = _context do
    #get the create a new address for this user
  #  address = "joshuaappiah116.mtngh.gh.lud" #%{"name" => "joshuaappiah116", "bag" => "GH", "wallet" => 11000023}
  #  return = Address.verify_account_address(address, X509.PublicKey.to_der(public), "GHA7eQUMce77kChpkoXHo2TT3nott")
  #  Logger.info("returned account address: #{inspect return}")
  #  :ok
  #end


end
