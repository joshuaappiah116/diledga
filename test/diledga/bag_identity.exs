defmodule Diledga.Country.Identity do
  use DiledgaWeb.ConnCase

  require Logger
  alias Diledga.Identity

  setup_all do
    on_exit(fn -> Diledga.Audit.DiskLogger.close_logger() end)
  end

  test "correct_list_country" do
    #create a country
    {:ok, bag} = Identity.create_bag(%{name: "Ghana", code: "GH"})

    #create a wallet
    wallet = %{address: "kdajfiejfakljfaeijflkajdsflk", code: 019384, name: "mtn ghana", public_name: "mtngh"}


    #bag_assoc = Ecto.build_assoc(bag, :wallets, wallet)
    {:ok, wallet} = Identity.create_wallet(wallet, bag)

    #set the user identification
    u1 = %{name: "joshuaappiah116"}
    {:ok, user} = Identity.create_user(u1, bag)

    #create an account with mtn ghana
    account = %{address: "dlafjdskfajlfkjlasdfkj", fullname: "joshuaappiah116.mtn.gh.lud", name: "joshuaappiah116"}
    {:ok, account} = Identity.create_account(account, user, wallet)
    Logger.warn("this is the list of documents obtained: \n#{inspect user}")
    Logger.warn("this is the list of documents obtained: \n#{inspect account}")

  end
end
