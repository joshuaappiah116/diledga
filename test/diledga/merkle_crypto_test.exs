defmodule Diledga.Merkle.CryptoTest do
  use ExUnit.Case

  require Logger
  alias Diledga.Merkle.Crypto
  alias Diledga.Merkle

  test "test_hash", _context do
    hash = Diledga.Merkle.Crypto.merkle_hash("joshua")
    Logger.debug("#{inspect hash}")
    :ok
  end

  test "run_merkle", _context do
    nodes =  ["a"]
    nodes = Enum.map(nodes, fn x -> Crypto.merkle_hash(x) end)
    {tree, root} = Merkle.merkle_tree(nodes)


    Logger.info("#{inspect Base.encode32(root.self)}")

    encodedTree = Enum.map(tree, fn x ->
      if x.leftnode == nil do
        %Diledga.Merkle.Node{self: Base.encode32(x.self, padding: false), height: x.height, leftnode: x.leftnode, rightnode: x.rightnode}
      else
        %Diledga.Merkle.Node{self: Base.encode32(x.self, padding: false), height: x.height, leftnode: Base.encode32(x.leftnode, padding: false), rightnode: Base.encode32(x.rightnode, padding: false)}
      end
    end)


    Logger.info("")
    Logger.info("")
    Logger.info("#{inspect encodedTree}")

    Logger.info("")
    Logger.info("")
    Logger.info("Length of the tree is: #{inspect length(encodedTree)}")

    Logger.info("")
    Logger.info("")
    Logger.info("#{inspect Base.encode32(List.last(tree).self)}")
    #test if the "b" is in the merkle tree
    Logger.info("")
    Logger.info("")
    bnode = Crypto.merkle_hash("e")
    valid? = Merkle.merkle_proof(tree, root.self, bnode)
    Logger.info("e is part of the tree #{inspect valid?}")

    Logger.info("")
    Logger.info("")
    bnode = Crypto.merkle_hash("a")
    valid? = Merkle.merkle_proof(tree, root.self, bnode)
    Logger.info("'a' is a member of the tree? #{inspect valid?}")

    Logger.info("")
    Logger.info("")
    bnode = Crypto.merkle_hash("b")
    valid? = Merkle.merkle_proof(tree, root.self, bnode)
    Logger.info("'b' is a member of the tree? #{inspect valid?}")

    Logger.info("")
    Logger.info("")
    bnode = Crypto.merkle_hash("c")
    valid? = Merkle.merkle_proof(tree, root.self, bnode)
    Logger.info("'c' is a member of the tree? #{inspect valid?}")

    Logger.info("")
    Logger.info("")
    bnode = Crypto.merkle_hash("d")
    valid? = Merkle.merkle_proof(tree, root.self, bnode)
    Logger.info("'d' is a member of the tree? #{inspect valid?}")

    Logger.info("")
    Logger.info("")
    bnode = Crypto.merkle_hash("f")
    valid? = Merkle.merkle_proof(tree, root.self, bnode)
    Logger.info("'f' is a member of the tree? #{inspect valid?}")

    Logger.info("")
    Logger.info("")
    bnode = Crypto.merkle_hash("g")
    valid? = Merkle.merkle_proof(tree, root.self, bnode)
    Logger.info("'g' is a member of the tree? #{inspect valid?}")

  end
end
