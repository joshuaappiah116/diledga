defmodule Diledga.BlockTest do
  use ExUnit.Case

  require Logger

  alias Diledga.Blockchain.BlockHeader
  alias Diledga.Blockchain.Block



  describe "check_blocks" do
    setup do
      privkey = X509.PrivateKey.new_ec(:secp256r1)
      pubkey = X509.PublicKey.derive(privkey)

      {:ok, pubkey: pubkey, privkey: privkey}
    end


    test "blockheader_test", context do
      header = %BlockHeader{}
      merkle =  :crypto.hash(:sha256, "The is the genesis block (sort of) !!!")

      signature = :public_key.sign(merkle, :sha256, context.privkey)
      binkey = X509.PublicKey.to_der(context.pubkey)
      Logger.info("We have the public key: #{inspect binkey}\n\n")

      bh =
        BlockHeader.set_timestamp(header)
        |> BlockHeader.set_version
        |> BlockHeader.set_authority(20)
        |> BlockHeader.set_merkle_root(Base.hex_encode32(merkle, padding: false, case: :lower))
        |> BlockHeader.set_block_certificate(context.privkey, merkle)
        |> BlockHeader.set_owner(binkey)

      #set the previous block header manually
      bh = %BlockHeader{bh | previousBlockHash: Base.hex_encode32(:crypto.hash(:sha256, merkle), padding: false, case: :lower)}

      #find the previous blockhash by going through all the required processes
      bh = BlockHeader.set_previousBlockHash(bh, bh)
      bh = Jason.encode!(bh)
      bh = Jason.decode!(bh)
      bh = BlockHeader.to_atom_map(bh)
      Logger.info("#{inspect bh}")
    end
  end

end
