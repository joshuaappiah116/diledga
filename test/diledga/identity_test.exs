defmodule Diledga.IdentityTest do
  use Diledga.DataCase

  alias Diledga.Identity

  describe "bags" do
    alias Diledga.Identity.Bag

    @valid_attrs %{code: "some code", name: "some name"}
    @update_attrs %{code: "some updated code", name: "some updated name"}
    @invalid_attrs %{code: nil, name: nil}

    def bag_fixture(attrs \\ %{}) do
      {:ok, bag} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Identity.create_bag()

      bag
    end

    test "list_bags/0 returns all bags" do
      bag = bag_fixture()
      assert Identity.list_bags() == [bag]
    end

    test "get_bag!/1 returns the bag with given id" do
      bag = bag_fixture()
      assert Identity.get_bag!(bag.id) == bag
    end

    test "create_bag/1 with valid data creates a bag" do
      assert {:ok, %Bag{} = bag} = Identity.create_bag(@valid_attrs)
      assert bag.code == "some code"
      assert bag.name == "some name"
    end

    test "create_bag/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Identity.create_bag(@invalid_attrs)
    end

    test "update_bag/2 with valid data updates the bag" do
      bag = bag_fixture()
      assert {:ok, %Bag{} = bag} = Identity.update_bag(bag, @update_attrs)
      assert bag.code == "some updated code"
      assert bag.name == "some updated name"
    end

    test "update_bag/2 with invalid data returns error changeset" do
      bag = bag_fixture()
      assert {:error, %Ecto.Changeset{}} = Identity.update_bag(bag, @invalid_attrs)
      assert bag == Identity.get_bag!(bag.id)
    end

    test "delete_bag/1 deletes the bag" do
      bag = bag_fixture()
      assert {:ok, %Bag{}} = Identity.delete_bag(bag)
      assert_raise Ecto.NoResultsError, fn -> Identity.get_bag!(bag.id) end
    end

    test "change_bag/1 returns a bag changeset" do
      bag = bag_fixture()
      assert %Ecto.Changeset{} = Identity.change_bag(bag)
    end
  end

  describe "wallets" do
    alias Diledga.Identity.Wallet

    @valid_attrs %{address: "some address", code: 42, name: "some name", public_name: "some public_name"}
    @update_attrs %{address: "some updated address", code: 43, name: "some updated name", public_name: "some updated public_name"}
    @invalid_attrs %{address: nil, code: nil, name: nil, public_name: nil}

    def wallet_fixture(attrs \\ %{}) do
      {:ok, wallet} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Identity.create_wallet()

      wallet
    end

    test "list_wallets/0 returns all wallets" do
      wallet = wallet_fixture()
      assert Identity.list_wallets() == [wallet]
    end

    test "get_wallet!/1 returns the wallet with given id" do
      wallet = wallet_fixture()
      assert Identity.get_wallet!(wallet.id) == wallet
    end

    test "create_wallet/1 with valid data creates a wallet" do
      assert {:ok, %Wallet{} = wallet} = Identity.create_wallet(@valid_attrs)
      assert wallet.address == "some address"
      assert wallet.code == 42
      assert wallet.name == "some name"
      assert wallet.public_name == "some public_name"
    end

    test "create_wallet/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Identity.create_wallet(@invalid_attrs)
    end

    test "update_wallet/2 with valid data updates the wallet" do
      wallet = wallet_fixture()
      assert {:ok, %Wallet{} = wallet} = Identity.update_wallet(wallet, @update_attrs)
      assert wallet.address == "some updated address"
      assert wallet.code == 43
      assert wallet.name == "some updated name"
      assert wallet.public_name == "some updated public_name"
    end

    test "update_wallet/2 with invalid data returns error changeset" do
      wallet = wallet_fixture()
      assert {:error, %Ecto.Changeset{}} = Identity.update_wallet(wallet, @invalid_attrs)
      assert wallet == Identity.get_wallet!(wallet.id)
    end

    test "delete_wallet/1 deletes the wallet" do
      wallet = wallet_fixture()
      assert {:ok, %Wallet{}} = Identity.delete_wallet(wallet)
      assert_raise Ecto.NoResultsError, fn -> Identity.get_wallet!(wallet.id) end
    end

    test "change_wallet/1 returns a wallet changeset" do
      wallet = wallet_fixture()
      assert %Ecto.Changeset{} = Identity.change_wallet(wallet)
    end
  end

  describe "users" do
    alias Diledga.Identity.User

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Identity.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Identity.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Identity.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Identity.create_user(@valid_attrs)
      assert user.name == "some name"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Identity.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Identity.update_user(user, @update_attrs)
      assert user.name == "some updated name"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Identity.update_user(user, @invalid_attrs)
      assert user == Identity.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Identity.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Identity.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Identity.change_user(user)
    end
  end

  describe "users" do
    alias Diledga.Identity.User

    @valid_attrs %{address: "some address", name: "some name"}
    @update_attrs %{address: "some updated address", name: "some updated name"}
    @invalid_attrs %{address: nil, name: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Identity.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Identity.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Identity.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Identity.create_user(@valid_attrs)
      assert user.address == "some address"
      assert user.name == "some name"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Identity.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Identity.update_user(user, @update_attrs)
      assert user.address == "some updated address"
      assert user.name == "some updated name"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Identity.update_user(user, @invalid_attrs)
      assert user == Identity.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Identity.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Identity.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Identity.change_user(user)
    end
  end

  describe "accounts" do
    alias Diledga.Identity.Account

    @valid_attrs %{address: "some address", name: "some name"}
    @update_attrs %{address: "some updated address", name: "some updated name"}
    @invalid_attrs %{address: nil, name: nil}

    def account_fixture(attrs \\ %{}) do
      {:ok, account} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Identity.create_account()

      account
    end

    test "list_accounts/0 returns all accounts" do
      account = account_fixture()
      assert Identity.list_accounts() == [account]
    end

    test "get_account!/1 returns the account with given id" do
      account = account_fixture()
      assert Identity.get_account!(account.id) == account
    end

    test "create_account/1 with valid data creates a account" do
      assert {:ok, %Account{} = account} = Identity.create_account(@valid_attrs)
      assert account.address == "some address"
      assert account.name == "some name"
    end

    test "create_account/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Identity.create_account(@invalid_attrs)
    end

    test "update_account/2 with valid data updates the account" do
      account = account_fixture()
      assert {:ok, %Account{} = account} = Identity.update_account(account, @update_attrs)
      assert account.address == "some updated address"
      assert account.name == "some updated name"
    end

    test "update_account/2 with invalid data returns error changeset" do
      account = account_fixture()
      assert {:error, %Ecto.Changeset{}} = Identity.update_account(account, @invalid_attrs)
      assert account == Identity.get_account!(account.id)
    end

    test "delete_account/1 deletes the account" do
      account = account_fixture()
      assert {:ok, %Account{}} = Identity.delete_account(account)
      assert_raise Ecto.NoResultsError, fn -> Identity.get_account!(account.id) end
    end

    test "change_account/1 returns a account changeset" do
      account = account_fixture()
      assert %Ecto.Changeset{} = Identity.change_account(account)
    end
  end
end
